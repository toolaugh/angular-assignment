# FeAssignment

 

This project was developed by Angular: 10.1.6 with the following libraries:
    `"@angular/animations": "~8.2.11",  
    "@angular/cdk": "~8.2.3",
    "@angular/common": "~8.2.11",
    "@angular/compiler": "~8.2.11",
    "@angular/core": "~8.2.11",
    "@angular/forms": "~8.2.11",
    "@angular/material": "^8.2.3",
    "@angular/platform-browser": "~8.2.11",
    "@angular/platform-browser-dynamic": "~8.2.11",
    "@angular/router": "~8.2.11",
    "bootstrap": "^4.5.3",`

## Development server



Run `ng serve` for running local. Navigate to `http://localhost:4200/`. if you have any new change with your the source files, this application will automatically update.

 

## Setup and run
1. insall`npm install`
2. start developement `ng serve`

 

## Program structure
1. Layout:
 In Layout folder is root layout that use for all project;
2. Modules
 In module folder, there is only one module is Test ()
 In test module, there is components folder that list all components used in test module
3. Router:
About router, there is only 1 screen so the router will navigate directly to the test module
4. Assets:
 Assets folder contains: imanges, icon, font, and scss 
 Mock `transactions.json` is placed in this folder 
5. Environments
 Define invironment for developement and deployment

## Functionality
1. Make transactions
2. List recent transactions 
3. Search transactions
4. Filter transactions by: `date`, `beneficary`, `amount`

I am looking forward to your feedback about my assignment. Please review & give me your advice. 